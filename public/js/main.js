

$(document).ready(function() {
    /* basic variables */
    var i; // for looping

    /* scroll magic initialize */
    var controller = new ScrollMagic.Controller();

    /* gallery initialize */
    $("#gallery-stage").unitegallery({
        slider_scale_mode: 'down'
    });

    /* scroll to function */
    controller.scrollTo(function (newpos) {
        TweenMax.to(window, 1.5, {scrollTo: {y: newpos - 80}, ease: Expo.easeInOut});
    });

    /* force scroll to top on refresh */
    //controller.scrollTo('#top');

    /* navbar */
    /* scroll to links */
    $(document).on("click", ".page-scroll", function (e) {
        var id = $(this).attr("href");
        if ($(id).length > 0) {
            e.preventDefault();
            controller.scrollTo(id);
        }
    });
    // close responsive menu on item click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });
    /* set id of navbar lines */
    i = 0;
    $( ".current-page" ).each(function() {
        $(this).attr('id', 'cp-'+i);
        i++;
    });
    /* add line on scroll */
    i = 0;
    $( "section" ).each(function() {
        var id = $(this).attr('id');
        var timeline = new TimelineMax();
        if (i > 0) {
            timeline.add(TweenMax.to('#cp-'+(i-1), 0.2, {
                x: '100%'
            }));
        } else {
            timeline.add(TweenMax.from('.navbar-right .page-scroll', 0.2, {
                paddingTop: 20
            }));
        }
        timeline.add(TweenMax.to('#cp-'+i, 0.2, {
            width: '100%'
        }));
        new ScrollMagic.Scene({
            triggerElement: '#'+id
        })
            .setTween(timeline)
            .addTo(controller);

        i++;
    });
    /* resize on scroll down */
    /* hide scroll down message on scroll down */
    var timeline = new TimelineMax()
        .add(TweenMax.to('header .next', 0.5, {
            opacity: 0
        }), 0)
        .add(TweenMax.from('nav', 0.5, {
            paddingTop: 5,
            paddingBottom: 5,
            backgroundColor: 'rgba(255,255,255,0)'
        }), 0)
        .add(TweenMax.to('.navbar-logo', 0.5, {
            scale: 1.2,
            x: 10
        }), 0);
    new ScrollMagic.Scene({
        offset: 100,
        duration: 0,
        triggerHook: 'onLeave'
    })
        .setTween(timeline)
        .addTo(controller);

    // parallax section breaks
    $( ".parallax" ).each(function() {
        new ScrollMagic.Scene({
            triggerElement: $(this),
            triggerHook: "0.9",
            duration: "200%"
        })
            .setTween($(this).children(), {y: "50%"})
            .addTo(controller);
    });

    // connectivity issue animation
    timeline = new TimelineMax()
        .add(TweenMax.from(".issue-anim-2", 1, {
            y: '-100%'
        }))
        .add(TweenMax.from(".issue-anim-1", 1, {
            y: '100%'
        }), 0)
        .add(TweenMax.from(".issue-anim-3", 0.2, {
            opacity: 0
        }))
        .add(TweenMax.to(".issue-anim-3", 0.2, {
            opacity: 1
        }))
        .add(TweenMax.to(".issue-anim-3", 0.2, {
            opacity: 0
        }))
        .add(TweenMax.to(".issue-anim-3", 0.2, {
            opacity: 1
        }));
    new ScrollMagic.Scene({
        duration: 0,
        triggerElement: "#issue",
        triggerHook: '0.4'
    })
        .setTween(timeline)
        .addTo(controller);

    // statistics animation
    timeline = new TimelineMax()
        .add(TweenMax.from(".stats-anim-2", 1, {
            opacity: 0
        }));
    new ScrollMagic.Scene({
        duration: 0,
        triggerElement: "#stats",
        triggerHook: '0.4'
    })
        .setTween(timeline)
        .addTo(controller);

    // cause animation
    for (i = 1; i <= 8; i++) {
        $(".lint-stage").append('<img class="anim lint" src="img/lint/' + i + '.png"/>')
    }
    timeline = new TimelineMax();
    $( ".lint" ).each(function() {
        timeline.add(TweenMax.to($(this), 0.2, {
            scale: 0.4,
            x: Math.random() * 100 - 125,
            y: Math.random() * 200 - 125
        }));
    });
    new ScrollMagic.Scene({
        duration: 0,
        triggerElement: "#cause"
    })
        .setTween(timeline)
        .addTo(controller);

    // picking animation
    timeline = new TimelineMax()
        .add(TweenMax.from(".pic-anim-1", 1, {
            y: '100%'
        }))
        .add(TweenMax.to(".pic-anim-1", 1, {
            y: '30px'
        }))
        .add(TweenMax.to(".pic-anim-2", 1, {
            y: '30px'
        }), 1);
    new ScrollMagic.Scene({
        duration: 0,
        triggerElement: "#pic",
        triggerHook: '0.4'
    })
        .setTween(timeline)
        .addTo(controller);

    // other devices animation
    timeline = new TimelineMax()
        .add(TweenMax.to(".others-anim-1", 1, {
            x: '150%'
        }))
        .add(TweenMax.to(".others-anim-2", 1, {
            x: '-5%'
        }), 0);
    new ScrollMagic.Scene({
        duration: 0,
        triggerElement: "#others",
        triggerHook: '0.4'
    })
        .setTween(timeline)
        .addTo(controller);

    // description animation
    timeline = new TimelineMax();
    $( ".desc-item" ).each(function() {
        timeline.add(TweenMax.from($(this), 0.2, {
            opacity: 0,
            rotationY: 90
        }));
    });
    new ScrollMagic.Scene({
        duration: 0,
        triggerElement: "#desc",
        triggerHook: '0.7'
    })
        .setTween(timeline)
        .addTo(controller);

    // easter egg & credits
    // konami code for credits section
    var Konami = function (callback) {
        var konami = {
            addEvent: function (obj, type, fn, ref_obj) {
                if (obj.addEventListener)
                    obj.addEventListener(type, fn, false);
                else if (obj.attachEvent) {
                    // IE
                    obj["e" + type + fn] = fn;
                    obj[type + fn] = function () {
                        obj["e" + type + fn](window.event, ref_obj);
                    };
                    obj.attachEvent("on" + type, obj[type + fn]);
                }
            },
            input: "",
            pattern: "38384040373937396665",
            load: function (link) {
                this.addEvent(document, "keydown", function (e, ref_obj) {
                    if (ref_obj) konami = ref_obj; // IE
                    konami.input += e ? e.keyCode : event.keyCode;
                    if (konami.input.length > konami.pattern.length)
                        konami.input = konami.input.substr((konami.input.length - konami.pattern.length));
                    if (konami.input == konami.pattern) {
                        konami.code(link);
                        konami.input = "";
                        e.preventDefault();
                        return false;
                    }
                }, this);
                this.iphone.load(link);
            },
            code: function (link) {
                window.location = link
            },
            iphone: {
                start_x: 0,
                start_y: 0,
                stop_x: 0,
                stop_y: 0,
                tap: false,
                capture: false,
                orig_keys: "",
                keys: ["UP", "UP", "DOWN", "DOWN", "LEFT", "RIGHT", "LEFT", "RIGHT", "TAP", "TAP"],
                code: function (link) {
                    konami.code(link);
                },
                load: function (link) {
                    this.orig_keys = this.keys;
                    konami.addEvent(document, "touchmove", function (e) {
                        if (e.touches.length == 1 && konami.iphone.capture == true) {
                            var touch = e.touches[0];
                            konami.iphone.stop_x = touch.pageX;
                            konami.iphone.stop_y = touch.pageY;
                            konami.iphone.tap = false;
                            konami.iphone.capture = false;
                            konami.iphone.check_direction();
                        }
                    });
                    konami.addEvent(document, "touchend", function (evt) {
                        if (konami.iphone.tap == true) konami.iphone.check_direction(link);
                    }, false);
                    konami.addEvent(document, "touchstart", function (evt) {
                        konami.iphone.start_x = evt.changedTouches[0].pageX;
                        konami.iphone.start_y = evt.changedTouches[0].pageY;
                        konami.iphone.tap = true;
                        konami.iphone.capture = true;
                    });
                },
                check_direction: function (link) {
                    x_magnitude = Math.abs(this.start_x - this.stop_x);
                    y_magnitude = Math.abs(this.start_y - this.stop_y);
                    x = ((this.start_x - this.stop_x) < 0) ? "RIGHT" : "LEFT";
                    y = ((this.start_y - this.stop_y) < 0) ? "DOWN" : "UP";
                    result = (x_magnitude > y_magnitude) ? x : y;
                    result = (this.tap == true) ? "TAP" : result;

                    if (result == this.keys[0]) this.keys = this.keys.slice(1, this.keys.length);
                    if (this.keys.length == 0) {
                        this.keys = this.orig_keys;
                        this.code(link);
                    }
                }
            }
        }

        typeof callback === "string" && konami.load(callback);
        if (typeof callback === "function") {
            konami.code = callback;
            konami.load();
        }

        return konami;
    };
    new Konami(function() {
        for (var i = 1; i < 22; i++) {
            $("body").append('<img class="konami" style="position: fixed; left: -'+(10+(Math.random() * 90))+'%; top:'+(Math.random() * 100)+'%" src="img/lint/'+i+'.png" />');
        }
        TweenMax.to($('.konami'), 5, {
            rotation: 1080,
            left: '150%'
        }).eventCallback("onStart", function() {
            var snd = new Audio('sound/whee.wav');
            snd.play();
        }).eventCallback("onComplete", function() {
            $('.konami').remove();
        });

        // credits
        console.log("=================== LintPic Website Credits ===================");
        console.log("Developer: Jaryl Chng");
        console.log("Designers: Taylor Tan and Daphne Goh");
        console.log("Assets: Nikole Tay, Joshua Ow Yong, Tan Zhi Ping, Ivan Cheong");
        console.log("Javascript plugins: ScrollMagic, GSAP, PACE and unitegallery");
        console.log("=================== LintPic Website Credits ===================");
    });
});